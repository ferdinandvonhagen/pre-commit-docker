FROM rust:1.66.0-buster

RUN apt-get update && \
    apt-get install -y python3-pip git pkg-config libssl-dev openssh-client apt-transport-https ca-certificates gnupg software-properties-common wget && \
    wget -O - https://apt.kitware.com/keys/kitware-archive-latest.asc 2>/dev/null | apt-key add - && \
    apt-add-repository 'deb https://apt.kitware.com/ubuntu/ bionic main' && \
    apt-get remove -y gnupg software-properties-common && \
    apt-get clean all && \
    apt-get update && \
    apt-get install -y cmake gcc-arm-none-eabi libclang-dev clang python python-pip && \
    pip3 install pre-commit && \
    rustup target add thumbv7em-none-eabihf && \
    rustup component add rustfmt && \
    rustup component add clippy && \
    cargo install cbindgen && \
    cargo install cargo-get && \
    eval $(ssh-agent -s) && \
    mkdir -p ~/.ssh && \
    chmod 700 ~/.ssh && \
    ssh-keyscan ssh.shipyard.rs >> ~/.ssh/known_hosts && \
    apt-get remove -y python-pip gnupg wget && \
    apt autoremove -y && \
    rm -rf /var/lib/apt/lists/*

